# LaTeX Presentation

A presentation about LaTeX using the [beamer](https://ctan.org/pkg/beamer) class.

## Licensing

Note: All logos belong to their respective owners and are used purely for
demonstration purposes.
